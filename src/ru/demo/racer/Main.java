package ru.demo.racer;

/**
 * Класс, демонстрирующий гонку потоков
 */
public class Main {
    /**
     * Метод запуска потоков.
     */
    public static void main(String[] args) {
        Racer racer1 = new Racer(10, 1);
        Racer racer2 = new Racer(1, 10);
        racer1.start();
        racer2.start();
    }
}
