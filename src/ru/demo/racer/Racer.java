package ru.demo.racer;

/**
 * Класс, демонстрирующий смену приоритета потока.
 * Содержит методы, изменяющий приоритет потока, инкрементирующий значение переменной.
 * На основе этого класса могут быть созданы объекты.
 */
public class Racer extends Thread {
    private int startPriority;
    private int finishPriority;

    /**
     * Получает начальное и конечное значения потока.
     * @param startPriority начальный приоритет потока.
     * @param finishPriority конечный приоритет потока.
     */
    Racer(int startPriority, int finishPriority) {
        this.startPriority = startPriority;
        this.finishPriority = finishPriority;
    }

    /**
     * Метод инкрементации счетчика и изменения приоритета потоков
     */
    @Override
    public void run() {
        setPriority(startPriority);
        for (int i = 0; i < 10000; i++) {
            System.out.println(getName() + "  " + getPriority());
            if (i == 5000) {
                setPriority(finishPriority);
            }
        }
    }


}
