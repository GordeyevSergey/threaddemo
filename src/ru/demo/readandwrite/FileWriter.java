package ru.demo.readandwrite;

import java.io.*;

/**
 * Класс записи данных в текстовый файл.
 */
public class FileWriter extends Thread {
    private static final String WAY_TO_RESULT = "src/ru/demo/ReadAndWrite/Result.dat";
    private String data;

    /**
     * Получает в качестве параметра строку.
     *
     * @param data строка данных типа String
     */
    FileWriter(String data) {
        this.data = data;
    }

    /**
     * Метод записи данных в текстовый файл
     */
    @Override
    public synchronized void run() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new java.io.FileWriter(WAY_TO_RESULT, true))) {
            bufferedWriter.write(data + "\n");
        } catch (IOException e) {
            System.out.println("ERROR" + e);
        }
    }
}
