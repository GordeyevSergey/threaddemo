package ru.demo.readandwrite;


/**
 * Проект, демонстрирующий многопоточное считывание из нескольких текстовых файлов
 * и многопоточную запись в результирующий текстовый файл.
 * <p>
 * Класс запуска потоков.
 */
public class Main extends Thread {

    /**
     * Метод создания и запуска потоков.
     */
    public static void main(String[] args) {
        Thread thread = new FileReader("src/ru/demo/ReadAndWrite/DataFile 1.dat");
        Thread thread1 = new FileReader("src/ru/demo/ReadAndWrite/DataFile 2.dat");
        thread.start();
        thread1.start();
    }

}
