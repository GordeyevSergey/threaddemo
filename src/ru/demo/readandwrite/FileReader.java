package ru.demo.readandwrite;

import java.io.*;

/**
 * Класс считывания данных из текстового файла.
 */
public class FileReader extends Thread {
    private String wayToData;

    /**
     * Получает в качестве параметра путь до считываемого файла.
     *
     * @param wayToData путь до считываемого файла.
     */
    FileReader(String wayToData) {
        this.wayToData = wayToData;
    }

    /**
     * Метод, осуществляющий считывание данных data типа String из текстового файла wayToData
     * Создает и запускает потоки записи данных в результирующий файл.
     * Замеряет время действия потоков программы.
     */
    @Override
    public void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(wayToData))) {
            final long timeBefore = System.currentTimeMillis();
            String data;
            while ((data = bufferedReader.readLine()) != null) {
                FileWriter threadOfWrite = new FileWriter(data);
                threadOfWrite.start();
            }
            System.out.println(getName() + "\n Time = " + (System.currentTimeMillis() - timeBefore) + "ms.");
        } catch (IOException e) {
            System.out.println("ERROR" + e);
        }
    }
}
