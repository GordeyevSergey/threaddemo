package ru.demo.downloader;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


 class LinkFileWriter {

    /**
      * Метод осуществляет поиск ссылок для скачивания в коде html страницы.
      *
      * @param htmlPageCode код страницы, на которой ведется поиск ссылок для скачивания
      */
     static void outFileWriter(String htmlPageCode, String OUT_FILE_TXT, String REGULAR, int NUMBER_OF_DOWNLOAD) {
         try (BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT, true))) {
             System.out.println("Ведется поиск ссылок на скачивание");
             Pattern email_pattern = Pattern.compile(REGULAR);
             Matcher matcher = email_pattern.matcher(htmlPageCode);
             for (int i = 0; matcher.find() && i < NUMBER_OF_DOWNLOAD; i++) {
                 outFile.write(matcher.group() + "\n");
             }
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
}
