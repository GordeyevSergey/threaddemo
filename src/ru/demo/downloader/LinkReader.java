package ru.demo.downloader;

import java.io.*;
import java.net.URL;
import java.util.stream.Collectors;


class LinkReader {

    /**
     * Метод считывает ссылки сайтов из файла IN_FILE_TXT.
     */
    static String siteLinkReader(String IN_FILE_TXT) {
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT))) {
            System.out.println("Осуществляется чтение ссылки");
            String result = null;
            String urlAddress;
            while ((urlAddress = inFile.readLine()) != null) {
                URL url = new URL(urlAddress);
                result = result + inputStreamReader(url);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Метод считывает исходный код html страниц по ссылке.
     *
     * @param url адрес сайта, где ведется поиск ссылок для скачивания музыки.
     * @return код html страницы.
     */
    private static String inputStreamReader(URL url) {
        String result = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
