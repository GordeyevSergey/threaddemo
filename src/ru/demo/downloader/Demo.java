package ru.demo.downloader;


import static ru.demo.downloader.LinkReader.*;

/**
 * Класс запуска главного потока программы.
 */
public class Demo {
    private static final String IN_FILE_TXT = "src\\ru\\demo\\downloader\\DownloadLink.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\demo\\downloader\\URL_Link.txt";
    private static final String pathToDownload = "src\\ru\\demo\\downloader\\Downloads\\file";
    private static final int NUMBER_OF_DOWNLOAD = 200; // Количество URL адресов, получаемых с 1 сайта.
    private static final String REGULAR = "(https:)?([/][a-z0-9._-]*)*(.jpg)";

    /**
     *
     */
    public static void main(String[] args){
                long currentTime = System.currentTimeMillis();
                String htmlCode = siteLinkReader( IN_FILE_TXT);
                LinkFileWriter.outFileWriter(htmlCode, OUT_FILE_TXT, REGULAR, NUMBER_OF_DOWNLOAD);
                DownloadThreadRunner.linkReader(OUT_FILE_TXT, pathToDownload);

                System.out.println("Время действия потока = " + ((System.currentTimeMillis() - currentTime) / 1000) + " сек.");
    }
}
