package ru.demo.downloader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


 class DownloadThreadRunner {

    /**
     * Метод считывает ссылки для скачивания файлов из текстового файла OUT_FILE_TXT.
     *
     */
    static void linkReader(String OUT_FILE_TXT, String pathToDownload) {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String music;
            for (int count = 0; (music = musicFile.readLine()) != null; count++) {
                Downloader downloader = new Downloader(music, pathToDownload + String.valueOf(count) + ".png");
                downloader.start();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
