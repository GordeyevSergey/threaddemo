package ru.demo.eggorchicken;

public class EggOrChicken extends Thread {
    private static final String EGG_VOICE = "Яйцо!";
    private static final String CHICKEN_VOICE = "Курица!";

    public static void main(String[] args) {
        MainThread egg = new MainThread(EGG_VOICE);
        MainThread chicken = new MainThread(CHICKEN_VOICE);
        egg.start();
        chicken.start();
    }

}