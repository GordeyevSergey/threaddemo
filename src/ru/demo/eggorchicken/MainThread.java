package ru.demo.eggorchicken;

public class MainThread extends Thread{

    private String name;

    MainThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(name);
        }
    }
}
