package ru.demo.copyfile;

import java.io.*;

/**
 * Класс, демонстрирующий копирование данных из текстового файла.
 */
public class CopyFile extends Thread {
    private String data;
    private String result;

    /**
     * Получает в качестве параметра пути до считываемого и результирующего файла.
     *
     * @param data   путь до считываемого файла.
     * @param result путь до результирующего файла.
     */
    CopyFile(String data, String result) {
        this.data = data;
        this.result = result;
    }

    /**
     * Получает в качестве параметра путь до считываемого файла.
     * Путь до результирующего файла устанавливается по умолчанию.
     *
     * @param data путь до считываемого файла.
     */
    CopyFile(String data) {
        this.data = data;
        this.result = "src\\ru\\demo\\copyfile\\Result.dat";
    }

    /**
     * Метод считывания данных из текстового файла data и записи их в файл result.
     * Осуществляет замер времени выполнения потока.
     */
    @Override
    public void run() {

        try (BufferedReader reader = new BufferedReader(new FileReader(data));
             BufferedWriter writer = new BufferedWriter(new FileWriter(result, true))) {
            String line;
            long startThreadTime = System.currentTimeMillis();
            while ((line = reader.readLine()) != null) {
                writer.write(line + "\n");
            }

            System.out.println("Время выполнения потока =  " + (System.currentTimeMillis() - startThreadTime));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
