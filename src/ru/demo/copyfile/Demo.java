package ru.demo.copyfile;

/**
 * Класс, запускающий поток
 */
public class Demo {
    public static void main(String[] args) {
        try {
            long startProgrammTime = System.currentTimeMillis();
            CopyFile thread = new CopyFile("src\\ru\\demo\\copyfile\\Data.dat", "src\\ru\\demo\\copyfile\\Result1.dat");
            CopyFile thread1 = new CopyFile("src\\ru\\demo\\copyfile\\Data.dat");
            thread.start();
            thread1.start();
            thread.join();
            thread1.join();
            System.out.println("Время выполнения программы =  " + (System.currentTimeMillis() - startProgrammTime));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
