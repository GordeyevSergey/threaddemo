package ru.demo.musicdownloader;

/**
 * Класс запуска главного потока программы.
 */
public class Demo {
    private static final String IN_FILE_TXT = "src\\ru\\demo\\musicdownloader\\DownloadLink.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\demo\\musicdownloader\\URL_Link.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\demo\\musicdownloader\\Downloads\\music";
    private static final int NUMBER_OF_DOWNLOAD = 4; // Количество URL адресов, получаемых с 1 сайта.

    /**
     *
     */
    public static void main(String[] args) {
        try {
            long currentTime = System.currentTimeMillis();
            MusicSearcher main = new MusicSearcher(IN_FILE_TXT, OUT_FILE_TXT, PATH_TO_MUSIC, NUMBER_OF_DOWNLOAD);
            main.run();
            main.join();
            System.out.println("Время действия потока = " + ((System.currentTimeMillis() - currentTime) / 1000) + " сек.");
        } catch (InterruptedException e) {
            System.out.println("ERROR \n" + e);
        }
    }
}
