package ru.demo.musicdownloader;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс осуществляет поиск ссылок для скачивания музыки.
 */
public class MusicSearcher extends Thread {
    private String IN_FILE_TXT;
    private String OUT_FILE_TXT;
    private String PATH_TO_MUSIC;
    private int NUMBER_OF_DOWNLOAD;

    /**
     * Получает пути до файлов, содержащих ссылки сайтов, ссылки для скачивания музыки,
     * а также путь до папки для скачивания и число, отвечающее за кол-во скачиваний с 1 сайта.
     *
     * @param IN_FILE_TXT        путь до файла с ссылками сайтов.
     * @param OUT_FILE_TXT       путь до файла, содержащего адреса для скачивания музыки.
     * @param PATH_TO_MUSIC      путь до папки с результатами скачивания.
     * @param NUMBER_OF_DOWNLOAD количество ссылок для скачивания, получаемых с 1 сайта.
     */
    MusicSearcher(String IN_FILE_TXT, String OUT_FILE_TXT, String PATH_TO_MUSIC, int NUMBER_OF_DOWNLOAD) {
        this.IN_FILE_TXT = IN_FILE_TXT;
        this.OUT_FILE_TXT = OUT_FILE_TXT;
        this.PATH_TO_MUSIC = PATH_TO_MUSIC;
        this.NUMBER_OF_DOWNLOAD = NUMBER_OF_DOWNLOAD;
    }

    /**
     * Метод считывает ссылки сайтов из файла IN_FILE_TXT.
     */
    @Override
    public void run() {
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT))) {
            System.out.println("Осуществляется чтение ссылки");
            String result, urlAddress;
            while ((urlAddress = inFile.readLine()) != null) {
                URL url = new URL(urlAddress);
                result = inputStreamReader(url);
                outFileWriter(result);
            }
            linkReader();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Метод осуществляет поиск ссылок для скачивания в коде html страницы.
     *
     * @param htmlPageCode код страницы, на которой ведется поиск ссылок для скачивания
     */
    private void outFileWriter(String htmlPageCode) {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT, true))) {
            System.out.println("Ведется поиск ссылок на скачивание");
            Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*/*(?=\")");
            Matcher matcher = email_pattern.matcher(htmlPageCode);
            for (int i = 0; matcher.find() && i < NUMBER_OF_DOWNLOAD; i++) {
                outFile.write(matcher.group() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод считывает исходный код html страниц по ссылке.
     *
     * @param url адрес сайта, где ведется поиск ссылок для скачивания музыки.
     * @return код html страницы.
     */
    private String inputStreamReader(URL url) {
        String result = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Метод считывает ссылки для скачивания музыки из текстового файла OUT_FILE_TXT.
     */
    private void linkReader() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String music;
            for (int count = 0; (music = musicFile.readLine()) != null; count++) {
                Downloader downloader = new Downloader(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                downloader.run();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

