package ru.demo.musicdownloader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс осуществляет скачивание файлов
 */
public class Downloader extends Thread {
    private String url;
    private String wayToFile;

    /**
     * Получает URL адрес и путь до файла.
     *
     * @param url       адрес скачиваемого файла.
     * @param wayToFile полный путь до файла.
     */
    Downloader(String url, String wayToFile) {
        this.url = url;
        this.wayToFile = wayToFile;
    }

    /**
     * Метод, осуществляющий скачивание файлов по ссылке url и сохраняет в wayToFile
     */
    @Override
    public void run() {
        try {
            URL url = new URL(this.url);
            try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
                 FileOutputStream stream = new FileOutputStream(wayToFile)) {
                System.out.println("Ведется скачивание файла " + wayToFile);
                stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException f) {
            f.printStackTrace();
        }
    }
}
