package ru.demo.increment;

/**
 * Класс запуска программы.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        new InterferenceExample().example();
    }
}