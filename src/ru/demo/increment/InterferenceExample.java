package ru.demo.increment;

import java.util.concurrent.atomic.AtomicInteger;

class InterferenceExample {
    private static final int HUNDRED_MILLION = 100_000_000;
    private AtomicInteger counter = new AtomicInteger();

    /**
     * Метод сравнивает текущее значение счетчика с целевым.
     * @return true, если счетчик counter меньше, чем HUNDRED_MILLION, иначе false.
     */
    boolean stop() {
        return counter.incrementAndGet() > HUNDRED_MILLION;
    }

    /**
     * Метод создания и запуска потоков.
     */
    void example() throws InterruptedException {
        InterferenceThread thread1 = new InterferenceThread("Поток 1", this);
        InterferenceThread thread2 = new InterferenceThread("Поток 2", this);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("Ожидаем: " + HUNDRED_MILLION);
        System.out.println("Получили: " + thread1.getI());
    }
}