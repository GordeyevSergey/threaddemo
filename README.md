## Репозиторий содержит проекты, демонстрирующие работу потоков. ##

#### Содержание: ####
### downloader ###
* Проект, осуществляющий скачивание файлов с сайтов.
* [Ссылка на проект](https://bitbucket.org/GordeyevSergey/threaddemo/src/b4a128c8d2817c0192b1da2295886f73a9840d1a/src/ru/demo/downloader/?at=master)

### musicdownloader ###
* Проект, осуществляющий скачивание музыки с сайтов.
* [Ссылка на проект](https://bitbucket.org/GordeyevSergey/threaddemo/src/1cb3a25703b7ac631e5045abc0632fe48a1c7a8d/src/ru/demo/musicdownloader/?at=master)

### racer ###
* Проект,осуществляющий гонку потоков.
* [Ссылка на проект](https://bitbucket.org/GordeyevSergey/threaddemo/src/20d5d64063c8812284c9f90a5fbb78a54ae5e9aa/src/ru/demo/racer/?at=master)

### copyfile ###
* Проект, осуществляющий копирование данных из текстовых файлов.
* [Ссылка на проект](https://bitbucket.org/GordeyevSergey/threaddemo/src/52420b71d69f81afc61e2223b46297bb6e1971d0/src/ru/demo/copyfile/?at=master)

### readandwrite ###
* Проект, демонстрирующий многопоточное считывание из нескольких текстовых файлов и многопоточную запись в результирующий текстовый файл.
* [Ссылка на проект](https://bitbucket.org/GordeyevSergey/threaddemo/src/9d41282776d635899c0df8db11179aa494b638a1/src/ru/demo/readandwrite/?at=master)

### increment ###
* Проект, осуществляющий многопоточное инкрементирование переменной.
* [Ссылка на проект](https://bitbucket.org/GordeyevSergey/threaddemo/src/f19adf0dc342b92fb3ba8c33c9211f4d02e7dce1/src/ru/demo/increment/?at=master)